// -------------
// RunDarwin.hpp
// -------------

#ifndef RunDarwin_h
#define RunDarwin_h

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <tuple>

#include "Darwin.hpp"

// -------------
// parse_problem
// -------------
/**
    * Parses an input stream into a Darwin problem
    * @param sin Input stream starting at line of form "R C"
    * @return A tuple containing the problem instance and dimensions
    */
tuple<Darwin, int, int> parse_problem(std::istream& sin);

#endif // RunDarwin_h
