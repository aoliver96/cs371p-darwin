// ----------
// Darwin.hpp
// ----------

#ifndef Darwin_h
#define Darwin_h

// --------
// includes
// --------

#include <cassert>  // assert
#include <list>     // list
#include <string>   // string
#include <utility>  // pair

#include "Creature.hpp"

using namespace std;

// ----------------
// Creature_Wrapper
// ----------------
struct Creature_Wrapper
{
    Creature c;
    pair<int, int> coordinates; //row major ordering
    char display;
    int uid;

    // ----------------
    // Creature_Wrapper
    // ----------------
    /**
    * @returns A Creature_Wrapper object with invalid coordinates, uid, and a '!' display.
    */
    Creature_Wrapper()
    {
        coordinates = pair<int, int>(-1, -1);
        display = '!';
        uid = -1;
    }

    // ----------------
    // Creature_Wrapper
    // ----------------
    /**
    * @param cr The Creature to be wrapped
    * @param row The row position of the Creature (0-indexed)
    * @param col The column position of the Creature (0-indexed)
    * @param disp The display character for the creature on the grid
    * @param id The unique ID to be assigned to this creature.
    * @returns A Creature_Wrapper object encoding a Creature's position, display character, and unique ID
    */
    Creature_Wrapper(Creature cr, int row, int col, char disp, int id)
    {
        c = cr;
        coordinates = pair<int, int>(row, col);
        display = disp;
        uid = id;
    }

    // ----------
    // operator==
    // ----------
    /**
    * @param rhs Creature_Wrapper object to compare equality of
    * @returns True if both Creature_Wrappers share a position, false otherwise.
    */
    bool operator==(Creature_Wrapper rhs)
    {
        return coordinates == rhs.coordinates;
    }

    // ---------
    // operator<
    // ---------
    /**
    * @param rhs Creature_Wrapper object to compare value to
    * @returns True if the calling Creature_Wrapper is before rhs in Row-Major Ordering, false otherwise.
    */
    bool operator<(Creature_Wrapper rhs)
    {
        return coordinates < rhs.coordinates;
    }
};

// ------
// Darwin
// ------
class Darwin
{

private:
    int row, col, id;
    list<Creature_Wrapper> creatures;
    Creature_Wrapper wall;
    Creature_Wrapper empty;
    string buffer;

    // ------------
    // check_facing
    // ------------
    /**
    * Determines what (if anything) occupies the grid space in front of a given Creature
    * @param current The creature whose position and direction determine the search
    * @param i An iterator containing current's position in creatures
    * @returns A reference to a Creature_Wrapper which current is facing. May be a static Wall or Empty Creature_Wrapper.
    */
    Creature_Wrapper& check_facing(Creature_Wrapper &current, list<Creature_Wrapper>::iterator i);

    // -------------
    // sorted_insert
    // -------------
    /**
    * Inserts a Creature_Wrapper into creatures, maintaining row-major ordering
    * @param c A valid Creature_Wrapper object.
    * @returns A bidirectional iterator pointing to the newly inserted Creature_Wrapper
    */
    list<Creature_Wrapper>::iterator sorted_insert(Creature_Wrapper& c);

public:


    // ------
    // Darwin
    // ------
    /**
    * @return A Darwin object with a 0*0 grid.
    */
    Darwin();

    // ------
    // Darwin
    // ------
    /**
    * @param r Row count
    * @param c Column
    * @return A Darwin object with an empty RxC grid.
    */
    Darwin(int r, int c);

    // ----------
    // print_grid
    // ----------
    /**
    * @return A string containing a formatted output of the current grid state.
    */
    string& print_grid();

    // --------
    // run_turn
    // --------
    /**
    * Runs a turn for every creature in the grid
    */
    void run_turn();

    // ------------
    // add_creature
    // ------------
    /**
    * Creates a creature at the position at r, c on the grid
    * @param r Row position
    * @param c Column position
    * @param species Initial species, character used as display character on grid.
    * @param dir Initial direction
    */
    void add_creature(int r, int c, char species, u_char dir);

    // ------------
    // add_creature
    // ------------
    /**
    * Adds an existing creature to the grid at r, c.
    * @param r Row position
    * @param c Column position
    * @param cr Creature to add to grid
    * @param char Display character for the creature on the grid.
    */
    void add_creature(int r, int c, Creature& cr, char display);
};
#endif // Darwin_h
