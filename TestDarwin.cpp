// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <memory>    // allocator
#include <sstream>   // istringstream
#include <string>    // string


#include "gtest/gtest.h"

#include "Darwin.hpp"
#include "RunDarwin.hpp"

using namespace std;

// --
// IO
// --

TEST(IOFixture, Parse_HR0)
{
    string input = "8 8\n6\nf 0 0 e\nh 3 3 n\nh 3 4 e\nh 4 4 s\nh 4 3 w\nf 7 7 n\n";
    istringstream sin(input);
    Darwin test_darwin = get<0>(parse_problem(sin));
    string expected_output = "  01234567\n0 f.......\n1 ........\n2 ........\n3 ...hh...\n4 ...hh...\n5 ........\n6 ........\n7 .......f\n";
    ASSERT_EQ(test_darwin.print_grid(), expected_output);
    test_darwin.run_turn();
    expected_output = "  01234567\n0 f.......\n1 ........\n2 ...h....\n3 .....h..\n4 ..h.....\n5 ....h...\n6 ........\n7 .......f\n";
    ASSERT_EQ(test_darwin.print_grid(), expected_output);
    test_darwin.run_turn();
    expected_output = "  01234567\n0 f.......\n1 ...h....\n2 ........\n3 ......h.\n4 .h......\n5 ........\n6 ....h...\n7 .......f\n";
    ASSERT_EQ(test_darwin.print_grid(), expected_output);
    test_darwin.run_turn();
    expected_output = "  01234567\n0 f..h....\n1 ........\n2 ........\n3 .......h\n4 h.......\n5 ........\n6 ........\n7 ....h..f\n";
    ASSERT_EQ(test_darwin.print_grid(), expected_output);
    test_darwin.run_turn();
    ASSERT_EQ(test_darwin.print_grid(), expected_output);
}

TEST(IOFixture, Parse_HR1)
{
    string input = "7 9\n4\nt 0 0 s\nh 3 2 e\nr 5 4 n\nt 6 8 w\n";
    istringstream sin(input);
    Darwin test_darwin = get<0>(parse_problem(sin));
    srand(0);
    string expected_output0 = "  012345678\n0 t........\n1 .........\n2 .........\n3 ..h......\n4 .........\n5 ....r....\n6 ........t\n";
    string expected_output1 = "  012345678\n0 t........\n1 .........\n2 .........\n3 ...h.....\n4 ....r....\n5 .........\n6 ........t\n";
    string expected_output2 = "  012345678\n0 t........\n1 .........\n2 .........\n3 ....r....\n4 ....r....\n5 .........\n6 ........t\n";
    string expected_output3 = "  012345678\n0 t........\n1 .........\n2 .........\n3 ....rr...\n4 .........\n5 .........\n6 ........t\n";
    string expected_output4 = "  012345678\n0 t........\n1 .........\n2 ....r....\n3 ......r..\n4 .........\n5 .........\n6 ........t\n";
    string expected_output5 = "  012345678\n0 t........\n1 ....r....\n2 .........\n3 .......r.\n4 .........\n5 .........\n6 ........t\n";
    ASSERT_EQ(test_darwin.print_grid(), expected_output0);
    test_darwin.run_turn();
    ASSERT_EQ(test_darwin.print_grid(), expected_output1);
    test_darwin.run_turn();
    ASSERT_EQ(test_darwin.print_grid(), expected_output2);
    test_darwin.run_turn();
    ASSERT_EQ(test_darwin.print_grid(), expected_output3);
    test_darwin.run_turn();
    ASSERT_EQ(test_darwin.print_grid(), expected_output4);
    test_darwin.run_turn();
    ASSERT_EQ(test_darwin.print_grid(), expected_output5);
}

// ------
// Darwin
// ------

TEST(DarwinFixture, Constructor_EmptyUnevenGrid)
{
    Darwin test_darwin(2, 3);
    string expected_output = "  012\n0 ...\n1 ...\n";
    /*  Expected output looks like:
        |  012
        |0 ...
        |1 ...
    */
    ASSERT_EQ(test_darwin.print_grid(), expected_output);
}

TEST(DarwinFixture, Constructor_PopulatedUnevenGrid)
{
    Darwin test_darwin(2, 3);
    test_darwin.add_creature(0, 1, 't', south);
    test_darwin.add_creature(1, 1, 'h', north);
    string expected_output = "  012\n0 .t.\n1 .h.\n";
    /*  Expected output looks like:
        |  012
        |0 .t.
        |1 .h.
    */
    ASSERT_EQ(test_darwin.print_grid(), expected_output);
}

TEST(DarwinFixture, Constructor_Overwrite)
{
    Darwin test_darwin(2, 3);
    test_darwin.add_creature(1, 1, 't', south);
    test_darwin.add_creature(1, 1, 'h', north);
    string expected_output = "  012\n0 ...\n1 .h.\n";
    /*  Expected output looks like:
        |  012
        |0 ...
        |1 .h.
    */
    ASSERT_EQ(test_darwin.print_grid(), expected_output);
}

TEST(DarwinFixture, Turn_Hop)
{
    Darwin test_darwin(2, 3);
    test_darwin.add_creature(1, 1, 'h', north);
    string expected_output = "  012\n0 ...\n1 .h.\n";
    /*  Expected output looks like:
        |  012
        |0 ...
        |1 .h.
    */
    ASSERT_EQ(test_darwin.print_grid(), expected_output);
    test_darwin.run_turn();
    expected_output = "  012\n0 .h.\n1 ...\n";
    /*  Expected output looks like:
        |  012
        |0 .h.
        |1 ...
    */
    ASSERT_EQ(test_darwin.print_grid(), expected_output);
    test_darwin.run_turn();
    ASSERT_EQ(test_darwin.print_grid(), expected_output);
}

TEST(DarwinFixture, Turn_Hop2)
{
    Darwin test_darwin(4, 4);
    test_darwin.add_creature(1, 1, 'h', north);
    test_darwin.add_creature(1, 2, 'h', east);
    test_darwin.add_creature(2, 1, 'h', west);
    test_darwin.add_creature(2, 2, 'h', south);
    string expected_output = "  0123\n0 ....\n1 .hh.\n2 .hh.\n3 ....\n";
    /*  Expected output looks like:
        |  0123
        |0 ....
        |1 .hh.
        |2 .hh.
        |3 ....
    */
    ASSERT_EQ(test_darwin.print_grid(), expected_output);
    test_darwin.run_turn();
    expected_output = "  0123\n0 .h..\n1 ...h\n2 h...\n3 ..h.\n";
    /*  Expected output looks like:
        |  0123
        |0 .h..
        |1 ...h
        |2 h...
        |3 ..h.
    */
    ASSERT_EQ(test_darwin.print_grid(), expected_output);
    test_darwin.run_turn();
    ASSERT_EQ(test_darwin.print_grid(), expected_output);
}

TEST(DarwinFixture, Turn_Infect)
{
    Darwin test_darwin(2, 3);
    test_darwin.add_creature(0, 1, 't', south);
    test_darwin.add_creature(1, 1, 'f', east);
    test_darwin.add_creature(1, 2, 'f', east);
    string expected_output = "  012\n0 .t.\n1 .ff\n";
    /*  Expected output looks like:
        |  012
        |0 .t.
        |1 .ff
    */
    ASSERT_EQ(test_darwin.print_grid(), expected_output);
    test_darwin.run_turn();
    expected_output = "  012\n0 .t.\n1 .tt\n";
    /*  Expected output looks like:
        |  012
        |0 .t.
        |1 .tt
    */
    ASSERT_EQ(test_darwin.print_grid(), expected_output);
}

TEST(DarwinFixture, Print_WideInput)
{
    Darwin test_darwin(15, 15);
    string expected_output = "  012345678901234\n0 ...............\n1 ...............\n2 ...............\n3 ...............\n4 ...............\n5 ...............\n6 ...............\n7 ...............\n8 ...............\n9 ...............\n0 ...............\n1 ...............\n2 ...............\n3 ...............\n4 ...............\n";
    /*  Expected output looks like:
          012345678901234
        0 ...............
        1 ...............
        2 ...............
        3 ...............
        4 ...............
        5 ...............
        6 ...............
        7 ...............
        8 ...............
        9 ...............
        0 ...............
        1 ...............
        2 ...............
        3 ...............
        4 ...............
    */
    ASSERT_EQ(test_darwin.print_grid(), expected_output);
}

TEST(DarwinFixture, Constructor_Reference)
{
    Darwin test_darwin(2, 3);
    Creature food('f', south);
    test_darwin.add_creature(0, 0, food, 'H');
    test_darwin.add_creature(0, 1, food, 'i');
    test_darwin.add_creature(0, 2, food, '!');
    string expected_output = "  012\n0 Hi!\n1 ...\n";
    /*  Expected output looks like:
        |  012
        |0 Hi!
        |1 ...
    */
    ASSERT_EQ(test_darwin.print_grid(), expected_output);
}

// --------
// Creature
// --------

TEST(CreatureFixture, Constructor_Wall)
{
    Creature test_creature('w', north);
    ASSERT_EQ(test_creature.is_wall(), true);
    ASSERT_EQ(test_creature.get_direction(), north);
}

TEST(CreatureFixture, Act_Food)
{
    Creature food('f', north);
    ASSERT_EQ(food.act(nullptr), left_);
    ASSERT_EQ(food.get_direction(), west);
}

TEST(CreatureFixture, Act_Hopper)
{
    Creature hopper('h', north);
    ASSERT_EQ(hopper.act(nullptr), hop);
}

TEST(CreatureFixture, Act_Rover)
{
    Creature rover('r', north);
    Creature prey('f', south);
    Creature wall('w', east);
    Creature rover2('r', west);
    srand(0);
    ASSERT_EQ(rover.act(nullptr), hop);
    ASSERT_EQ(rover.act(&wall), right_);
    ASSERT_EQ(rover.get_direction(), east);
    ASSERT_EQ(rover.act(&rover2), left_);
    ASSERT_EQ(rover.get_direction(), north);
    ASSERT_EQ(rover.act(&prey), infect);
    ASSERT_TRUE(species_equal(prey, rover));
}

TEST(CreatureFixture, Act_Trap)
{
    Creature trap('t', north);
    Creature prey('f', south);
    Creature wall('w', east);
    Creature trap2('t', west);

    ASSERT_EQ(trap.act(nullptr), left_);
    ASSERT_EQ(trap.get_direction(), west);
    ASSERT_EQ(trap.act(&wall), left_);
    ASSERT_EQ(trap.get_direction(), south);
    ASSERT_EQ(trap.act(&trap2), left_);
    ASSERT_EQ(trap.get_direction(), east);
    ASSERT_EQ(trap.act(&prey), infect);
    ASSERT_TRUE(species_equal(prey, trap));
}

TEST(CreatureFixture, Constructor_Species)
{
    Species s('t');
    Creature test_creature(s, north);
    Creature food('f', north);
    ASSERT_EQ(test_creature.is_wall(), false);
    ASSERT_EQ(test_creature.get_direction(), north);
    ASSERT_EQ(test_creature.act(nullptr), left_);
    ASSERT_EQ(test_creature.act(&food), infect);
}

// -------
// Species
// -------

TEST(SpeciesFixture, Act_Food)
{
    Species test_species('f');
    int pc = 0;
    ASSERT_EQ(test_species.act(pc, 0), left_);
    ASSERT_EQ(pc, 1);
    ASSERT_EQ(test_species.act(pc, 0), left_);
    ASSERT_EQ(pc, 1);
}

TEST(SpeciesFixture, Act_Hopper)
{
    Species test_species('h');
    int pc = 0;
    ASSERT_EQ(test_species.act(pc, 0), hop);
    ASSERT_EQ(pc, 1);
    ASSERT_EQ(test_species.act(pc, 0), hop);
    ASSERT_EQ(pc, 1);
}

TEST(SpeciesFixture, Act_Rover)
{
    Species test_species('r');
    int pc = 0;
    srand(0);
    //if_enemy
    ASSERT_EQ(test_species.act(pc, 3), infect);
    ASSERT_EQ(pc, 10);
    //if_empty
    ASSERT_EQ(test_species.act(pc, 0), hop);
    ASSERT_EQ(pc, 8);
    //if_random(odd)
    ASSERT_EQ(test_species.act(pc, 1), right_);
    ASSERT_EQ(pc, 6);
    //if_random(even)
    ASSERT_EQ(test_species.act(pc, 1), left_);
    ASSERT_EQ(pc, 4);
}

TEST(SpeciesFixture, Act_Trap)
{
    Species test_species('t');
    int pc = 0;
    //if_enemy
    ASSERT_EQ(test_species.act(pc, 3), infect);
    ASSERT_EQ(pc, 4);
    //!if_enemy
    ASSERT_EQ(test_species.act(pc, 0), left_);
    ASSERT_EQ(pc, 2);
}

TEST(SpeciesFixture, Equality_True)
{
    Species test_species('t');
    Species test_species2('t');
    ASSERT_TRUE(test_species == test_species2);
}

TEST(SpeciesFixture, Equality_False)
{
    Species test_species('t');
    Species test_species2('h');
    ASSERT_FALSE(test_species == test_species2);
}

TEST(SpeciesFixture, AddInstruction)
{
    Species test_species;
    Species trap('t');
    test_species.add_instruction(if_enemy, 3);
    test_species.add_instruction(left_);
    test_species.add_instruction(go, 0);
    test_species.add_instruction(infect);
    test_species.add_instruction(go, 0);

    ASSERT_EQ(test_species, trap);
}
