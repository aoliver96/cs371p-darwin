// -----------
// Species.hpp
// -----------

#ifndef Species_h
#define Species_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <stdexcept> // invalid_argument
#include <vector>
#include <string>

using instr = std::pair<int, int>;
using instr_set = std::vector<instr>;

enum instruction_list : int
{
    hop, left_, right_, infect, if_empty, if_wall, if_random, if_enemy, go
};

// ---------------------
// Pre-Defined Creatures
// ---------------------

static const instr_set food_instr({instr(left_, 0), instr(go, 0)});
static const instr_set hopper_instr({instr(hop, 0), instr(go,0)});
static const instr_set rover_instr({instr(if_enemy, 9), instr(if_empty, 7), instr(if_random, 5), instr(left_, 0), 
                        instr(go, 0), instr(right_, 0), instr(go, 0), instr(hop, 0), instr(go, 0), instr(infect, 0), instr(go,0)});
static const instr_set trap_instr({instr(if_enemy, 3), instr(left_, 0), instr(go, 0), instr(infect, 0), instr(go, 0)});

// -------
// Species
// -------

class Species
{

private:
    instr_set instruction_set;

public:
    // -------
    // Species
    // -------
    /**
    * @returns A Species object with an empty (but initialized) instruction set.
    */
    Species();

    // -------
    // Species
    // -------
    /**
    * @param type The type of pre-defined species to create (w, f, t, h, r).
    * @returns A Species object with a pre-defined instruction set, or an empty set if the type is unrecognized.
    */
    Species(char type);

    // -------------
    // print_species
    // -------------
    /**
    * Debug method which prints the Species' instruction list to clog.
    */
    void print_instr();

    // ----------
    // operator==
    // ----------
    /**
    * @param lhs The first Species object to compare.
    * @param rhs The second Species object to compare.
    * @returns True if both species contain the same instruction set, false otherwise.
    */
    friend bool operator==(Species lhs, Species rhs);

    // ---
    // act
    // ---
    /**
    * @param pc The program counter of the calling creature. Automatically updated during operation.
    * @param front Contents of the front grid space. 0 = empty, 1 = wall, 2 = friend, 3 = enemy
    * @return Action to be performed. 0 = Hop, 1 = left_, 2 = right_, 3 = Infect
    */
    int act(int& pc, int front);

    // ---------------
    // add_instruction
    // ---------------
    /**
    * @param i Instruction type
    * @param jmp Jump location for instruction. Defaults to 0.
    * @throws "Invalid instruction." if instruction is not in the enumerated list.
    */
    void add_instruction(int i, int jmp = 0);

};

#endif // Species_h
