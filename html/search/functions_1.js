var searchData=
[
  ['check_5ffacing',['check_facing',['../classDarwin.html#a6551153fccfd863bd658be0b13830210',1,'Darwin']]],
  ['creature',['Creature',['../classCreature.html#a597cc3b08ee17de46c3e7ec3cf0d9b58',1,'Creature::Creature()'],['../classCreature.html#ae7c7a5d134518688244b322a4203f10b',1,'Creature::Creature(char type, u_char dir)'],['../classCreature.html#a211c46d12de08293755b5452f7335e7a',1,'Creature::Creature(Species &amp;s, u_char dir)']]],
  ['creature_5fwrapper',['Creature_Wrapper',['../structCreature__Wrapper.html#a0ace9d6e1cc9ecc625873f7de7021966',1,'Creature_Wrapper::Creature_Wrapper()'],['../structCreature__Wrapper.html#a32d54892d46725d07f77a881e5c2eb5a',1,'Creature_Wrapper::Creature_Wrapper(Creature cr, int row, int col, char disp, int id)']]]
];
