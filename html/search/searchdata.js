var indexSectionsWithContent =
{
  0: "abcdefghilmnoprstuw",
  1: "cds",
  2: "cdrst",
  3: "acdfghilmoprstu",
  4: "bcdeiprsuw",
  5: "i",
  6: "di",
  7: "eghilnrsw",
  8: "os",
  9: "c"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "related",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Friends",
  9: "Pages"
};

