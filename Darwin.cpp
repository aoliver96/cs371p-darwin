// ----------
// Darwin.cpp
// ----------

// --------
// includes
// --------

#include <cassert>          // assert
#include <iostream>         // endl, istream, ostream
#include <sstream>          // istringstream
#include <string>           // getline, string
#include <list>             // list
#include <vector>           // vector
#include <utility>          // pair
#include <unordered_set>    // set


#include "Darwin.hpp"
#include "Creature.hpp"
#include "Species.hpp"

using namespace std;

// ------
// Darwin
// ------
Darwin::Darwin()
{
    row = 0;
    col = 0;
    id = 0;
    wall = Creature_Wrapper(Creature('w', -1), -1, -1, 'w', -1);
}

// ------
// Darwin
// ------
Darwin::Darwin(int rows, int cols)
{
    row = rows;
    col = cols;
    id = 0;
    wall = Creature_Wrapper(Creature('w', -1), -1, -1, 'w', -1);
}

// ----------
// print_grid
// ----------
string& Darwin::print_grid()
{
    list<Creature_Wrapper>::iterator cwi = creatures.begin();
    list<Creature_Wrapper>::iterator _e = creatures.end();

    buffer = "  ";
    buffer.reserve((2+col)*(row+1));

    //set x-axis
    for (int i = 0; i < col; ++i)
    {
        buffer += to_string(i%10);
    }
    buffer += "\n";

    //print row by row
    for (int i = 0; i < row; ++i)
    {
        buffer += to_string((i%10)) + " ";
        if (cwi != _e && cwi->coordinates.first == i) //row has creature in it
        {
            for(int j = 0; j < col; ++j)
            {
                if (cwi != _e && cwi->coordinates.first == i && cwi->coordinates.second == j)
                {
                    buffer += cwi->display;
                    ++cwi;
                }
                else
                    buffer += '.';
            }
        }
        else    // row has no creatures in it, print empty line
        {
            buffer += string(col, '.');
        }
        buffer += "\n";
    }

    return buffer;
}

// -------------
// sorted_insert
// -------------

list<Creature_Wrapper>::iterator Darwin::sorted_insert(Creature_Wrapper& cw)
{
    if (!creatures.empty())
    {
        // Halve search space by going backwards
        if(cw.coordinates.first > row/2)
        {
            list<Creature_Wrapper>::reverse_iterator _r = creatures.rbegin();
            while (_r != creatures.rend())
            {
                if (*_r < cw)
                    return creatures.insert(_r.base(), cw);
                if (cw == *_r) // if equal to next, replace it
                {
                    *_r = cw;
                    return (++_r).base();
                }
                ++_r;
            }
            creatures.push_front(cw);
            return creatures.begin();
        }
        else
        {
            list<Creature_Wrapper>::iterator i = creatures.begin();
            while (i != creatures.end())
            {
                if(cw < *i)
                    return creatures.insert(i, cw);
                if (cw == *i) // if equal to next, replace it
                {
                    *i = cw;
                    return i;
                }
                ++i;
            }
            creatures.push_back(cw);
            return --(creatures.end());
        }
    }
    else
    {
        creatures.push_back(cw);
        return creatures.begin();
    }
    cerr << "Reached end of sorted_insert(), creature position = (" << cw.coordinates.first << ", " << cw.coordinates.second << ")\n";
    assert(false);
}

// ------------
// add_creature
// ------------
void Darwin::add_creature(int r, int c, char species, u_char dir)
{
    if( r >= row || r < 0  || c >= col || c < 0)
        throw "Creature out of bounds!";
    if (dir > 3)
        throw "Invalid direction!";

    Creature_Wrapper cw(Creature(species, dir), r, c, species, ++id);
    sorted_insert(cw);
}

// ------------
// add_creature
// ------------
void Darwin::add_creature(int r, int c, Creature& cr, char display)
{
    if (r >= row || r < 0 || c >= col || c < 0)
    {
        throw "Creature out of bounds!";
    }

    Creature_Wrapper cw(cr, r, c, display, ++id);
    sorted_insert(cw);
}

// --------------
// check_facing()
// --------------

Creature_Wrapper& Darwin::check_facing(Creature_Wrapper &current, list<Creature_Wrapper>::iterator i)
{

    int cur_row = current.coordinates.first;
    int cur_col = current.coordinates.second;

    switch (current.c.get_direction())
    {
    case north:
    {
        if (cur_row == 0)
        {
            return wall;
        }
        list<Creature_Wrapper>::reverse_iterator _r(++i);
        while (++_r != creatures.rend())
        {
            if (_r->coordinates.first >= cur_row) // Not far enough
                continue;
            if (_r->coordinates.first < cur_row - 1) // Too far
                break;
            if (_r->coordinates.first == cur_row - 1 && _r->coordinates.second == cur_col) // Just right
                return *_r;
        }
        break;
    }

    case east:
    {
        if (cur_col + 1 == col)
            return wall;
        if (++i != creatures.end() && i->coordinates.first == cur_row && i->coordinates.second == cur_col + 1)
            return *i;
        break;
    }

    case south:
    {
        if (cur_row == row - 1)
            return wall;
        while (++i != creatures.end())
        {
            if (i->coordinates.first <= cur_row) // Not far enough
                continue;
            if (i->coordinates.first > cur_row + 1) // Too far
                break;
            if (i->coordinates.first == cur_row + 1 && i->coordinates.second == cur_col) // Just right
                return *i;
        }
        break;
    }

    case west:
    {
        if (cur_col == 0)
            return wall;
        if (i != creatures.begin() && (--i)->coordinates.first == cur_row && i->coordinates.second == cur_col - 1)
            return *i;
        break;
    }

    default:
    {
        cerr << "check_facing() has encountered an invalid direction: " << current.c.get_direction() << "\n";
        assert(false);
    }
    }
    return empty;
}

// --------
// run_turn
// --------
void Darwin::run_turn()
{
    unordered_set<int> moved;
    int action;

    for (list<Creature_Wrapper>::iterator i = creatures.begin(); i != creatures.end();)
    {
        Creature_Wrapper& current = *i;

        if(moved.find(current.uid) == moved.end())
        {
            moved.insert(current.uid);
            Creature_Wrapper& front = check_facing(current, i);


            if (front.display == '!')
                action = current.c.act(nullptr);
            else
            {
                if (front.display == 'w')
                    action = current.c.act(&wall.c);
                else
                    action = current.c.act(&front.c);
            }

            switch (action)
            {
            case hop:
                if(front.display == '!')
                {
                    list<Creature_Wrapper>::iterator hop_itr = i++; // iterate i forward before copying to avoid invalidation
                    switch (current.c.get_direction())
                    {
                    case north:
                    {
                        --current.coordinates.first;
                        Creature_Wrapper tmp = current;
                        creatures.erase(hop_itr);
                        sorted_insert(tmp);
                        break;
                    }
                    case south:
                    {
                        ++current.coordinates.first;
                        Creature_Wrapper tmp = current;
                        creatures.erase(hop_itr);
                        sorted_insert(tmp);
                        break;
                    }
                    // East and West won't ever adjust the relative positions of the elements
                    case east:
                    {
                        ++hop_itr->coordinates.second;
                        break;
                    }
                    case west:
                    {
                        --hop_itr->coordinates.second;
                        break;
                    }
                    default:
                    {
                        cerr << "run_turn() has encountered an invalid direction: " << current.c.get_direction() << "\n";
                        assert(false);
                    }
                    }
                }
                else
                    ++i;
                break;

            case infect:
            {
                front.display = current.display;
                ++i;
                break;
            }
            default:
            {
                ++i;
                break;
            }
            }
        }
        else
        {
            ++i;
        }
    }
    return;
}
