// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <string>   // string
#include <cstdlib>
#include <tuple>    // tuple

#include "Darwin.hpp"
using namespace std;

// -------------
// parse_problem
// -------------

tuple<Darwin, int, int> parse_problem(istream& sin)
{

    string line;
    // Get World Size

    int r;
    sin >> r;
    int c;
    sin >> c;

    // read the rest of the line off the stream
    getline(sin, line);

    // Get Creatures
    int n;
    sin >> n;
    getline(sin, line);
    Darwin problem_instance(r, c);

    for (; n > 0; --n)
    {
        char tok, direction;
        int creature_r, creature_c;
        sin >> tok;

        sin.ignore(1);
        sin >> creature_r;
        sin.ignore(1);
        sin >> creature_c;
        sin.ignore(1);
        sin >> direction;

        switch (direction)
        {
        case 'n':
            direction = north;
            break;
        case 'e':
            direction = east;
            break;
        case 's':
            direction = south;
            break;
        case 'w':
            direction = west;
            break;
        default:
            direction = 0;
        }

        problem_instance.add_creature(creature_r, creature_c, tok, direction);

    }

    return make_tuple(problem_instance, r, c);

}

#ifndef Testing //Compiler ignores this Main when running the test file, and uses GTest Main

// ----
// main
// ----

int main()
{
    string line, output;
    int problems = 0;
    int step, freq;

    // parse problem input for number of Darwin instances
    getline(cin, line);
    problems = stoi(line);

    // read the newline off the stream
    getline(cin, line);

    tuple<Darwin, int, int> problem_tuple;
    Darwin current_problem;

    for (; problems > 0; --problems)
    {
        srand(0);
        problem_tuple = parse_problem(cin);
        current_problem = get<0>(problem_tuple);
        //Get Simulation Parameters

        cin >> step;
        cin >> freq;

        output += "*** Darwin " + to_string(get<1>(problem_tuple)) + "x" + to_string(get<2>(problem_tuple)) + " ***\n";
        output += "Turn = 0.\n";
        output += current_problem.print_grid() + "\n";
        if(step/freq >= 1)
        {
            for (int i = 1; i <= step; ++i)
            {
                current_problem.run_turn();
                if(i%freq == 0)
                {
                    output += "Turn = " + to_string(i) + ".\n";
                    output += current_problem.print_grid() + "\n";
                }
            }
        }
    }
    output.erase(output.length()-1);
    cout << output;
    return 0;
}
#endif //Testing
