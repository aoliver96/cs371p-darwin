// ------------
// Species.cpp
// ------------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <string>   // getline, string
#include <vector>   // vector
#include <utility>  // pair
#include <stdlib.h> // rand

#include "Species.hpp"

using namespace std;

// -------
// Species
// -------
Species::Species()
{
    instruction_set = {};
}

// -------
// Species
// -------
Species::Species(char type)
{
    switch (type)
    {
    case 'h':
        instruction_set = hopper_instr;
        break;

    case 'r':
        instruction_set = rover_instr;
        break;

    case 't':
        instruction_set = trap_instr;
        break;

    case 'f':
        instruction_set = food_instr;
        break;
    default:
        instruction_set = {};
    }
}

// -----------
// print_instr
// -----------
void Species::print_instr()
{
    clog << "Printing current instruction set:\n";
    instr_set::iterator i = instruction_set.begin();
    while (i != instruction_set.end())
    {
        clog << i->first << ", " << i->second << "\n";
        ++i;
    }
}

// ----------
// operator==
// ----------
bool operator==(Species lhs, Species rhs)
{
    return lhs.instruction_set == rhs.instruction_set;
}

// ---
// act
// ---
int Species::act(int& pc, int front)
{
    while(true)
    {
        assert((u_int)pc < instruction_set.size()); // Ward against a malformed instruction sending the pc OOB
        switch (instruction_set[pc].first)
        {
            case hop:
                ++pc;
                return hop;

            case left_:
                ++pc;
                return left_;

            case right_:
                ++pc;
                return right_;

            case infect:
                ++pc;
                return infect;

            case if_empty:
                if (!front)
                {
                    pc = instruction_set[pc].second;
                    break;
                }
                ++pc;
                break;

            case if_wall:
                if(front == 1)
                {
                    pc = instruction_set[pc].second;
                    break;
                }
                ++pc;
                break;

            case if_random:
                if(rand()%2)
                {
                    pc = instruction_set[pc].second;
                    break;
                }
                ++pc;
                break;

            case if_enemy:
                if(front == 3)
                {
                    pc = instruction_set[pc].second;
                    break;
                }
                ++pc;
                break;
                
            case go:
                pc = instruction_set[pc].second;
                break;
        }
    }
}

// ---------------
// add_instruction
// ---------------
void Species::add_instruction(int i, int j)
{
    if(i < 0 || i > 8)
        throw "Invalid instruction.";
    instruction_set.push_back(instr(i, j));
}