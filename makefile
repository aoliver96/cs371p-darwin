.DEFAULT_GOAL := all
MAKEFLAGS     += --no-builtin-rules
SHELL         := bash

ASTYLE        := astyle
CHECKTESTDATA := checktestdata
CPPCHECK      := cppcheck
DOXYGEN       := doxygen
VALGRIND      := valgrind

ifeq ($(shell uname -s), Darwin)
    BOOST    := /usr/local/include/boost
    CXX      := g++-10
    CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -I/usr/local/include -Wall -Wextra
    GCOV     := gcov-10
    GTEST    := /usr/local/include/gtest
    LDFLAGS  := -lgtest -lgtest_main
    LIB      := /usr/local/lib
else ifeq ($(shell uname -p), unknown)
    BOOST    := /usr/include/boost
    CXX      := g++
    CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -Wall -Wextra
    GCOV     := gcov
    GTEST    := /usr/include/gtest
    LDFLAGS  := -lgtest -lgtest_main -pthread
    LIB      := /usr/lib
else
    BOOST    := /usr/include/boost
    CXX      := g++-9
    CXXFLAGS := --coverage -pedantic -std=c++17 -O3 -Wall -Wextra
    GCOV     := gcov-9
    GTEST    := /usr/local/include/gtest
    LDFLAGS  := -lgtest -lgtest_main -pthread
    LIB      := /usr/local/lib
endif

# get git config
config:
	git config -l

# get git log
Darwin.log:
	git log > Darwin.log

# compile run harness
RunDarwin: Species.hpp Creature.hpp Darwin.hpp Species.cpp Creature.cpp Darwin.cpp RunDarwin.cpp 
	-$(CPPCHECK) Species.cpp Creature.cpp Darwin.cpp
	-$(CPPCHECK) RunDarwin.cpp
	$(CXX) $(CXXFLAGS) Species.cpp Creature.cpp Darwin.cpp RunDarwin.cpp -o RunDarwin

# compile test harness
TestDarwin: Species.hpp Creature.hpp Darwin.hpp Species.cpp Creature.cpp Darwin.cpp TestDarwin.cpp RunDarwin.cpp
	-$(CPPCHECK) Species.cpp Creature.cpp Darwin.cpp RunDarwin.cpp
	-$(CPPCHECK) TestDarwin.cpp
	$(CXX) $(CXXFLAGS) Species.cpp Creature.cpp Darwin.cpp TestDarwin.cpp RunDarwin.cpp -DTesting -o TestDarwin $(LDFLAGS)

# run/test files, compile with make all
FILES :=		\
    RunDarwin	\
    TestDarwin	

# compile all
all: $(FILES)

# check integrity of input file
ctd-check:
	$(CHECKTESTDATA) RunDarwin.ctd RunDarwin.in

# generate a random input file
ctd-generate:
	$(CHECKTESTDATA) -g RunDarwin.ctd RunDarwin.tmp

# execute run harness and diff with expected output
run: RunDarwin
	./RunDarwin < RunDarwin.in > RunDarwin.tmp
	-diff -s RunDarwin.tmp RunDarwin.out

# execute test harness
test: TestDarwin
	$(VALGRIND) ./TestDarwin
	$(GCOV) -b Darwin.cpp | grep -B 2 "cpp.gcov"

# clone the Darwin test repo
Darwin-tests:
	git clone https://gitlab.com/gpdowning/cs371p-Darwin-tests.git Darwin-tests

# test files in the Darwin test repo
TFILES := `ls Darwin-tests/*.in`

# execute run harness against a test in Darwin test repo and diff with expected output
Darwin-tests/%: RunDarwin
	./RunDarwin < $@.in > RunDarwin.tmp
	-diff -s RunDarwin.tmp $@.out

# execute run harness against all tests in Darwin test repo and diff with expected output
tests: Darwin-tests RunDarwin
	-for v in $(TFILES); do make $${v/.in/}; done

# auto format the code
format:
	$(ASTYLE) Darwin.cpp
	$(ASTYLE) Darwin.hpp
	$(ASTYLE) RunDarwin.cpp
	$(ASTYLE) TestDarwin.cpp

# you must edit Doxyfile and
# set EXTRACT_ALL     to YES
# set EXTRACT_PRIVATE to YES
# set EXTRACT_STATIC  to YES
# create Doxfile
Doxyfile:
	$(DOXYGEN) -g

# create html directory
html: Doxyfile Darwin.hpp
	$(DOXYGEN) Doxyfile

# check files, check their existence with make check
CFILES :=          \
    .gitignore     \
    .gitlab-ci.yml \
    Darwin.log    \
    html

# check the existence of check files
check: $(CFILES)

# remove executables and temporary files
clean:
	rm -f *.gcda
	rm -f *.gcno
	rm -f *.gcov
	rm -f *.plist
	rm -f *.tmp
	rm -f RunDarwin
	rm -f TestDarwin
	rm -f output.txt

# remove executables, temporary files, and generated files
scrub:
	make clean
	rm -f  Darwin.log
	rm -f  Doxyfile
	rm -rf Darwin-tests
	rm -rf html
	rm -rf latex

# output versions of all tools
versions:
	@echo "% shell uname -p"
	@echo  $(shell uname -p)
	@echo
	@echo "% shell uname -s"
	@echo  $(shell uname -s)
	@echo
	@echo "% which $(ASTYLE)"
	@which $(ASTYLE)
	@echo
	@echo "% $(ASTYLE) --version"
	@$(ASTYLE) --version
	@echo
	@echo "% which $(CHECKTESTDATA)"
	@which $(CHECKTESTDATA)
	@echo
	@echo "% $(CHECKTESTDATA) --version"
	@$(CHECKTESTDATA) --version
	@echo
	@echo "% which cmake"
	@which cmake
	@echo
	@echo "% cmake --version"
	@cmake --version
	@echo
	@echo "% which $(CPPCHECK)"
	@which $(CPPCHECK)
	@echo
	@echo "% $(CPPCHECK) --version"
	@$(CPPCHECK) --version
	@echo
	@$(CXX) --version
	@echo "% which $(DOXYGEN)"
	@which $(DOXYGEN)
	@echo
	@echo "% $(DOXYGEN) --version"
	@$(DOXYGEN) --version
	@echo
	@echo "% which $(CXX)"
	@which $(CXX)
	@echo
	@echo "% $(CXX) --version"
	@$(CXX) --version
	@echo
	@echo "% which $(GCOV)"
	@which $(GCOV)
	@echo
	@echo "% $(GCOV) --version"
	@$(GCOV) --version
	@echo
	@echo "% which git"
	@which git
	@echo
	@echo "% git --version"
	@git --version
	@echo
	@echo "% grep \"#define BOOST_LIB_VERSION \" $(BOOST)/version.hpp"
	@grep "#define BOOST_LIB_VERSION " $(BOOST)/version.hpp
	@echo
	@echo "% cat $(GTEST)/README"
	@cat $(GTEST)/README
	@echo
	@echo "% ls -al $(LIB)/libgtest*.a"
	@ls -al $(LIB)/libgtest*.a
	@echo
	@echo "% which make"
	@which make
	@echo
	@echo "% make --version"
	@make --version
ifneq ($(shell uname -s), Darwin)
	@echo
	@echo "% which $(VALGRIND)"
	@which $(VALGRIND)
	@echo
	@echo "% $(VALGRIND) --version"
	@$(VALGRIND) --version
endif
	@echo "% which vim"
	@which vim
	@echo
	@echo "% vim --version"
	@vim --version
