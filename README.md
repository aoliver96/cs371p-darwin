# CS371p: Object-Oriented Programming Darwin Repo

* Name: Alex Oliver

* EID: AJO695

* GitLab ID: aoliver96

* HackerRank ID: oliver_alex96

* Git SHA: 55100c7ea495e37291848e748753277b2c506fbe

* GitLab Pipelines: https://gitlab.com/aoliver96/cs371p-darwin/-/pipelines

* Estimated completion time: 15hr

* Actual completion time: 33.5hr

* Comments:

This took drastically longer than I expected it to because my initial Darwin.cpp implementation was 
based upon a pointer-heavy 2d vector implementation, and I was running into memory corruption bugs 
with it. Overhauling the Darwin.cpp code to instead use an implied grid and simply storing things 
in a list took a decent amount of time.

I also realized after I submitted to HackerRank that I have a couple of easy optimizations I could
grab when creating a Darwin instance by simply adding all the items to the list then sorting it,
which would cut the O(n^2) creation time down to O(nlogn), but c'est la vie, it works as is.

The requirement to have a direction attribute be part of the Creature class feels like a giant
wrench in the design space, since basically everything else about the grid position is held by
by Darwin, and it basically necessitated a getter or double-storing the information.

Attributions:
    - The CheckTestData schema is primarily based on Orion Reynold's piazza entry, with minor 
        modifications made to generate a test file of acceptable size.
