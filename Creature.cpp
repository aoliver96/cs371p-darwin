// ------------
// Creature.cpp
// ------------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <list>     // list
#include <vector>   // vector

#include "Creature.hpp"
#include "Species.hpp"

using namespace std;

// --------
// Creature
// --------
Creature::Creature()
{
    direction = north;
    program_counter = 0;
    wall = false;
}

// --------
// Creature
// --------
Creature::Creature(char type, u_char dir)
{
    program_counter = 0;
    direction = dir;

    if(type == 'w')
        wall = true;
    else
    {
        wall = false;
        s = Species(type);
    }
}

// --------
// Creature
// --------
Creature::Creature(Species& sp, u_char dir)
{
    program_counter = 0;
    direction = dir;
    s = sp;
    wall = false;
}

// ----
// left
// ----
void Creature::left()
{
    (--direction) %= 4;
}

// -----
// right
// -----
void Creature::right()
{
    (++direction) %= 4;
}

// --------------
// update_species
// --------------
void Creature::update_species(Species& new_species)
{
    program_counter = 0;
    s = new_species;
}

// -------------
// species_equal
// -------------
bool species_equal(Creature &lhs, Creature &rhs)
{
    return lhs.s==rhs.s;
}

// ---
// act
// ---
int Creature::act(Creature* front)
{
    int action;
    if(front == nullptr)
        action = s.act(program_counter, 0);
    else
    {
        if (front->is_wall())
            action = s.act(program_counter, 1);
        else
        {
            if (species_equal(*this, *front))
                action = s.act(program_counter, 2);
            else
                action = s.act(program_counter, 3);
        }  
    }
    switch (action)
    {
    case hop:
        return hop;
    
    case left_:
        this->left();
        return left_;
    
    case right_:
        this->right();
        return right_;

    case instruction_list::infect:
        if(front != nullptr && !front->is_wall() && !species_equal(*this, *front))
        {
            front->update_species(s);
            return instruction_list::infect;
        }
        // If infect is called on an invalid target, do nothing.
        return -1;

    default:
        return -1;
    }
}

// -------------
// print_species
// -------------
void Creature::print_species()
{
    s.print_instr();
}
