// ------------
// Creature.hpp
// ------------

#ifndef Creature_h
#define Creature_h

// --------
// includes
// --------

#include <cassert>   // assert

#include "Species.hpp"

using namespace std;

// ------
// Creature
// --------

enum directions : int
{
    north, east, south, west
};

class Creature
{
private:
    u_char direction;
    int program_counter;
    Species s;
    bool wall; // This boolean is used to identify the wall creature which is passed to species for control instructions

    // ----
    // left
    // ----
    /**
    * Turns the creature to face its left.
    */
    void left();

    // -----
    // right
    // -----
    /**
    * Turns the creature to face its right.
    */
    void right();

public:

    // --------
    // Creature
    // --------
    /**
    * @return A default creature object with no Species, facing North.
    */
    Creature();

    // --------
    // Creature
    // --------
    /**
    * @param type Char corresponding to a Pre-Defined species type (w, f, h, r, t) or an empty species otherwise.
    * @param dir The starting direction of the Creature
    * @return A Creature with a Pre-Defined Species and provided direction.
    */
    Creature(char type, u_char dir);

    // --------
    // Creature
    // --------
    /**
    * @param s A Species object.
    * @param dir The starting direction of the Creature
    * @return A Creature using the provided Species object and direction.
    */
    Creature(Species& s, u_char dir);

    // -------------
    // update_speces
    // -------------
    /**
    * Update the species of the called Creature.
    * @param new_species The species object which will replace the Creature's existing Species
    */
    void update_species(Species& new_species);

    // -------------
    // species_equal
    // -------------
    /**
    * Compares the species of two Creatures.
    * @param lhs The first Creature to compare.
    * @param rhs The second Creature to compare.
    * @return True if the Species have identical instruction sets, false otherwise.
    */
    friend bool species_equal(Creature& lhs, Creature& rhs);

    // ---
    // act
    // ---
    /**
    * Ask the Species what Action to perform, performing it if capable.
    * @param front The Creature object occupying the grid space in front of the acting Creature. Expects a nullptr if the grid space is empty.
    * @return The Action to perform.
    */
    int act(Creature* front);

    // These getters are necessary for passing information out of the class, however no setters are required.

    // -------
    // is_wall
    // -------
    /**
    * @return True if the creature is a Wall, false otherwise.
    */
    bool is_wall(){return wall;}

    // -------------
    // get_direction
    // -------------
    /**
    * @return The direction element of the Creature.
    */
    u_char get_direction(){return direction;}

    // -------------
    // print_species
    // -------------
    /**
    * Debug function which prints the calling Creature's Species' instruction list to clog.
    */
    void print_species();
};
#endif // Creature_h
